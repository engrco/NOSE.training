---
title: Hyperspectral Imaging
subtitle: Using Multiple Layers of Imaging To Smell
date: 2022-01-25
tags: ["example", "bigimg"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---


If we want to sniff what is out there ... or if we develop practical applications of olfactory sensing, especially, if we want to work in the non-contact realm first with zero-sample preparation and no need to be physically next to the object [at least as at first] ... the work will involve a lot of signature analysis and longitudinal coordination of different layers of data from [hyperspectral imaging](https://en.wikipedia.org/wiki/Hyperspectral_imaging) ... which might sound really complicated, because maybe it gets that way fast.  

But, in essence it's a *simple* matter of looking at the electromagnetic signature in different spectra for the same object and re-coordinating the data ... as humans do with our coordination of sensing sight, sound, smell, taste, touch, movements to piece together a more unique characteristic signature ... over time we learn different telltalke facets of this signature so that we better, more immediately recognize objects, then animals, then people and loved ones ... it's all about TRAINING the NOSE.

As with a baby developing sensory percectioin, it takes time ... but the larger competency can be broken down into small projects, tasks and a a practical workflow of TRAINING -- the practical TRAINING of the AI/ML pipeline and workflow will consists of following tedious, perhaps even somewhat mundane, but highly specialized technical tasks:

* Design, develop, revise, edit, update, improve documentation, especially testing documentation ... this will extend to maintain data dashboards and various quality metrics for data-driven review of capabilites.

* Provide technical solutions to a wide range of complex testing problems ... which is really about communication with other technical specialists in a variety of applications domains ... most of the applications will need to be incorporated into other products -- accordingly, this will involve provide testing-related technical guidance to team members from other ventures and companies.

* Develop and maintain test scripts (automation and manual) for automating the development and testing software applications which are part of continuous integration into useful analytical notebooks [using Jupyter or similar frameworks] progressive web applications and products that utilize the imaging data.


This will require developing a modicum of competency in testing software applications and hardware systems (Windows, Linux, embedded) ... which depends upon familiarity with the complete range of agile product/software development testing activities such as designing, writing, testing, and documenting software test cases, and scripts in languages and environments at different levels, eg Integration, Component, System and Acceptance.

The work also extends to communication and interpersonal skills and fostering team collaborating by providing technical guidance to the various technical team members. Of course, this is necessarily a polyglot environment but it will be particularly important to have competence in Python, C/C++ and scripting languages/libraries, not necessarily limited to Javascript/Node but definitely familiarity in that area. Of course, knowledge of HSI camera systems: electronic and optics, SW, and applications will also be beneficial.
