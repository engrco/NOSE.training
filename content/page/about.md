---
title: Bioinformatics Training For Deep Learning
subtitle: Biosemiotics, Hormonal Response, Molecular Biology Techniques, Genetic Sequencing,
comments: false
---

NOSE training is about pre-cognitive or more fundamental, basic training in bioinformatics and the biosensors that will be used at the edge of bioinformatic data collections ... the edge is really about more intelligently collecting significantly more data than with [current techniques in molecular biology](https://en.wikipedia.org/wiki/Category:Molecular_biology_techniques).

NOSE.training is about theory ... specifically it is about the theoretical foundations of the applied deep learning that will be necessary for more ubiquitous, inexpensive and [intelligent] biosensors ... how would we build sniffing devices that would process more than several orders of magnitude more data than we now process current in biochemical and molecular biology laboratories.

## Toward a Theory of Sniffing at the Edge 

We might think about what a nose's tissues might sense ... think of tissues more well-developed than our own or even more developed the nose of any dog ... theoretically perfect nose tissue ...  before the tissues translate the first, most basic signal to the brain, before there is any cognition of sensing something or wanting to express what is sensed in facial expressions or body language, eg a turn of the head to bring the nose closer, an exclamation or yelp, before any signs or external indication ... way, way before words or a coherent thought.

[Biosemiotics](https://en.wikipedia.org/wiki/Biosemiotics) studies prelinguistic meaning-making in biological organisms involving the production and interpretation of signaling and communication in the biological realm in complex organisms and at the very lowest levels ... humans might attempt to articulate what they feel or think when they smell something or are aroused by pheromones, but biosemiotics is at a step below that conscious expression ... the field involves [phytosemiotics or vegetative semiotics](https://en.wikipedia.org/wiki/Phytosemiotics) as well as [zoosemiotics or animal semiotics](https://en.wikipedia.org/wiki/Zoosemiotics).

In addition to biosemiotics, NOSE.training also looks at biochemistry, hormonal response signatures, genetic sequencing ... the training process that we are interested is about the theory of training something like an intelligent biosensor to make sense of the patterns in the raw data. 