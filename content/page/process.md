---
title: Bioinformatics Navigation
subtitle: Biosemiotics Is A Process of Exploring LOT of Data and Mapping Our Way Around In That Data 
comments: false
---

The NOSE.training ***process*** is one of developing a theoretical framework ... developing a theory of training is tedious, groping, uncertain, failure-prone stuff ... how do we develop our senses before we even understand what senses are or why it matters to develop them ... involves a gigantic amount of perturbation and playing around with [multi-scale and multi-dimensional mapping of things like genonmic data](https://docs.higlass.io/) ... or even [just learning how to build a data visualization library and use D3 for Observable notebooks](https://observablehq.com/collection/@d3/learn-d3).

Our aim is to understand the *lay of the land* on a topology or terrain that is not native terrain. Moreover, we want to look at different scales, drilling down through several scales that will not be remotely familiar to [most] human beings ... but getting this sort of hands-on familiarity with the data is like understanding the notion of mathematics by understanding basic geometric notions, ie how most of us probably initially developed our understanding of what a derivative is or its importance, such as when the derivative or slope of a curve at a particular point is zero, we are at a local minimum/maximum.