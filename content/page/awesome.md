---
title: Currated AWESOME Reading Lists
subtitle: The NOSErxiv of Sensor Development
comments: false
---

We READ a lot ... partly, we read in order improve our reading skills and the technologies we use to read ... quite often, as theorists, we are only concerned with thinking about constructs and ideas, but sometimes we really want to read to learn about practical, down-to-earth lab stuff, such as [the methods and materials that some are using to examine how fly cells develop an olfactory sensing system](https://www.biorxiv.org/content/10.1101/2021.05.04.442682v1)